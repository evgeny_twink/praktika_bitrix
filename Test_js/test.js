
const questions = [
{
    question: 'Что такое ПХП?',
    answer: 'Классная штука!',
    variants: ['var1', 'Классная штука!', 'var_3', 'var_4']
},
{
    question: 'Что такое da?',
    answer: 'штука!',
    variants: ['var1', 'var2', 'штука!', 'var_4']
},
{
    question: 'Что такое net?',
    answer: 'Класс!',
    variants: ['var1', 'var2', 'var_3', 'Класс!']
},
{
    question: '2+2?',
    answer: '4',
    variants: ['4', 'var2', 'var_3', 'Класс!']
},
{
    question: '2=2?',
    answer: '2',
    variants: ['var1', '2', 'var_3', 'Класс!']
},
{
    question: '1-3?',
    answer: '-2',
    variants: ['var1', 'var2', 'var_3', '-2']
},
{
    question: 'dadada',
    answer: 'aga',
    variants: ['aga', 'var2', 'var_3', 'Класс!']
},
{
    question: 'stop?',
    answer: 'stop',
    variants: ['var1', 'var2', 'stop', 'Класс!']
},
{
    question: 'lol',
    answer: 'kekw',
    variants: ['kekw', 'var2', 'var_3', 'Класс!']
},
{
    question: 'Что такое .net?',
    answer: 'C#',
    variants: ['var1', 'C#', 'var_3', 'Класс!']
}
];

var i = 0;
var counter = 0;
function show(){
    if(i > 0){                              //Удалить старый вопрос и заменить новым
        var parent = document.getElementById("containertest");
        var child = document.getElementById("myId"+(i-1));
        parent.removeChild(child);
    }
    if(i == 0){                             //Удалить кнопку Start после начала
        var father = document.getElementById("containertest");
        var btn = document.getElementById("startbtn");
        father.removeChild(btn);
    }
    if(i <= 9){

    let div = document.createElement('div');                //add div
    div.setAttribute( "id", "myId"+i );
    div.setAttribute( "class", "myClass"+i );
    document.body.querySelector(".containertest").appendChild(div);
  
    let label = document.createElement('label');            //add label
    label.innerHTML = questions[i]['question'];
    document.body.querySelector(".myClass"+i).appendChild(label);

    for (var x = 0; x < 4; x++){                            //Создание по 4 кнопки на каждый вопрос
    let input = document.createElement('input');
    input.setAttribute( "type", "button" );
    input.setAttribute( "class", "btn btn-primary" );
    input.setAttribute( "id", questions[i]['variants'][x] );
    input.setAttribute( "onclick", "show()" );
    input.setAttribute( "value", questions[i]['variants'][x]);
    document.body.querySelector(".myClass"+i).appendChild(input);
    }
    
    document.getElementById(questions[i]['answer']).onclick = function() {    //Подсчет ответов
        counter++;
        show();
    };

    let count_quest = document.createElement('label');            //На каком сейчас вопросе x/10
    count_quest.innerHTML = i+1+"/10";
    count_quest.setAttribute( "class", "labeli" );
    document.body.querySelector(".myClass"+i).appendChild(count_quest);

    i++;
    }
    else{
        let input = document.createElement('input');    //Создать скрытый input с результатами
        input.setAttribute( "type", "hidden" );
        input.setAttribute( "name", "result" );
        input.setAttribute( "value", counter);
        document.body.querySelector(".formtest").appendChild(input);

        document.getElementById("containertest").remove();        //Удалить div с тестом
        document.getElementById('sendusertest').hidden = false;     //Открыть форму с отправкой email, name, result

    }
}