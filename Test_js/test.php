<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8" />
  <script type="text/javascript" src="test.js"></script>
  <link rel="stylesheet" href="../style.css">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <title>HTML5</title>
 </head>
 <body>
 
<div class="containertest" id="containertest"> 
    <button onclick="show()" style="width:500px;height:400px;" class="btn btn-success" id="startbtn">Start</button>
</div> 

 <div class="sendusertest" id="sendusertest" hidden>
 <form action="save_user_test.php" method="POST" id="formtest" class="formtest">
    <div class="form-group">
        <label for="">Email address</label>
        <input name="email" type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
      </div>
      <div class="form-group">
        <label for="">Name</label>
        <input name="name" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter name">
      </div>
      <br>
      <button type="submit" class="btn btn-primary">Submit</button>
 </form>
 </div>

 </body>
</html>