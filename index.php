<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>praktika &mdash; Coming Soon</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="This is a default index page for a new domain."/>
        <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<div class="sendresume" id="sendresume">
    <form method="POST" id="send" action="emailscript.php" enctype="multipart/form-data">
      <div class="form-group">
        <label for="">Email address</label>
        <input name="email" type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
      </div>
      <div class="form-group">
        <label for="">Name</label>
        <input name="name" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter name">
      </div>
      <div class="form-group">
        <label for="">Password</label>
        <input name="password" type="password" class="form-control" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="">Photo</label>
        <input name="file" type="file" class="form-control" accept=".jpg, .png, .jpeg">
      </div>
      <div class="form-group">
        <label for="">Resume</label>
        <input name="resume" type="file" class="form-control" accept=".doc, .txt, .pdf">
      </div>
      <br>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<!-- <script type="text/javascript">      //Аякс запрос
$("#send").submit(function(e){
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "emailscript.php",
        data: $("#send").serialize(),
        success: function(data) {
            $("#result").html(data);
        }
    });
});
</script>

<div id="result">       //Вывод ответа после аякса
    
</div> -->

</body>
</html>

