<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>praktika &mdash; Coming Soon</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="This is a default index page for a new domain."/>
        <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="../style_for_token.css" rel="stylesheet">
    <link href="../style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
  <div class="exit_form" style="margin-right:10px;">
  <a href="forget_pass.php" class="btn btn-warning">Забыл пароль</a>
  </div>
<div class="sendresume">
    <h1>Авторизация</h1>
    <form action="auth.php" method="POST">
      <div class="form-group">
        <label for="">Login</label>
        <input name="name" type="text" class="form-control" placeholder="Enter login">
      </div>
      <div class="form-group">
        <label for="">Password</label>
        <input name="pass" type="password" class="form-control" placeholder="Enter password">
      </div>
      <br>
      <a href="registration.php" class="btn btn-outline-primary" style="float:right; width:48%;">Регистрация</a>
      <button type="submit" class="btn btn-success" style="width:48%;">Войти</button>
    </form>
        <?php
        if($_POST){
            $name = $_POST['name'];
            $pass = $_POST['pass'];
            include("mysql_connect.php");
            $request = "SELECT * FROM `users`";
            $data = mysqli_query($link, $request);
            $sql = mysqli_fetch_all($data, 1);
            foreach($sql as $elem){
                if($name == $elem['name'] && $pass == $elem['pass']){
                    $token_cookie = mt_rand();
                    setcookie ("auth", md5($token_cookie), time()+3600);
                    header("Location: http://praktika.it-kolibri.com/bitbucketScript_formAdd/read_token_user.php");
                }
            }
        }
        ?>
</div>
</body>
</html>
