<?php if(!isset($_COOKIE['auth'])){header("Location: http://praktika.it-kolibri.com/bitbucketScript_formAdd/auth.php");} ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>praktika &mdash; Coming Soon</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="This is a default index page for a new domain."/>
        <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="../style_for_token.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
  <form action="exit_account.php" method="POST" class="exit_form">
    <button class="btn btn-warning">Выйти</button>
  </form>
<div class="wrapper">
    <a href="add_token_user.php"  class="btn btn-info">Добавить нового пользователя</a>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">User_id</th>
      <th scope="col">Token</th>
      <th scope="col">Email</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
      <?php
      include("mysql_connect.php");
      $request = "SELECT * FROM `user_and_token`";
      $data = mysqli_query($link, $request);
      $sql = mysqli_fetch_all($data, 1);
      foreach($sql as $elem){
          echo '
            <tr>
            <th scope="row">'.$elem['user_id'].'</th>
            <td>'.$elem['token'].'</td>
            <td>'.$elem['email'].'</td>
            <td>
            <form action="edit_token_user.php" method="POST">
            <input type="submit" value="Изменить" class="btn btn-success">
            <input type="hidden" value="'.$elem['id'].'" name="edit_ut">
            </form>
            </td>
            <td>
            <form action="delete_token_user.php" method="POST">
            <input type="submit" value="Удалить" class="btn btn-danger">
            <input type="hidden" value="'.$elem['id'].'" name="delete_ut">
            </form>
            </td>
            </tr> 
          ';}
    ?>
  </tbody>
</table>
</div>


</body>
</html>

