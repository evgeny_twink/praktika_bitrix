<?php
$arrEmployee = [];

include("mysql_connect.php");
$request = "SELECT * FROM `user_and_token`";      
$data = mysqli_query($link, $request);
$sql = mysqli_fetch_all($data, 1);
foreach($sql as $elem){
  array_push($arrEmployee, [$elem['user_id'], $elem['token']],);  //Вытащить и запушить в массив всех юзеров из бд
}

$debug = false;                                // Дебаг вкл/выкл
$text = json_decode(file_get_contents('php://input'), true);    //Получение объекта с bitbucket
$text = json_encode($text);

if($debug){                                 //Сам дебагер
  $log = date('Y-m-d H:i:s'." Текст сообщения: ") . $text;
  file_put_contents(__DIR__ . '/Tekst.txt', $log . PHP_EOL, FILE_APPEND);
}

$text = json_decode($text);
$name_branch = $text->push->changes[0]->old->name;   //Получение названия ветки с объекта
$message = $text->push->changes[0]->commits[0]->message;  //Получение сообщения с объекта
$name_user = $text->push->changes[0]->old->target->author->raw;
preg_match('/\<(.*)\>/ui', $name_user, $res); // res[1] = email

$urlSearch = file_get_contents('https://b24-v1vsle.bitrix24.ru/rest/1/y2yad3kveho3784h/user.search.json'); //user.search bitrix24 одна ссылка на все аккаунты
$linkSearch = json_decode($urlSearch);
foreach($linkSearch->result as $elem){
  if($elem->EMAIL === $res[1]){
    $email_id = $elem->ID;
  }
  else{
    $email_id = 1;
  }
}

for($i = 0; $i < count($arrEmployee); $i++){    //Проверка на совпадения email из bitbucket и mysql
  if($arrEmployee[$i][0] == $email_id){
    $token = $arrEmployee[$i][1];
  }
}

$arrTasks = [];
$arrMessages = [];

if (strpos($message, 'task_') !== false) {        //Если в сообщение есть task_№
  preg_match_all('/\[(task_\d+)\]/ui', $message, $tasks); //вытащить id 
  $arrMessages = preg_split('/\[task_\d+\]/ui', $message);//массив из сообщений    
  foreach($tasks[1] as $elem){
    array_push($arrTasks, str_replace("task_", "", $elem)); //Массив из номеров тасков
  }
  $arrMessages = array_diff($arrMessages, array(''));
  $arrMessages = array_values($arrMessages);
}
else{                                            //Если в сообщение НЕТ task_№
  preg_match('/task_(\d+)/ui', $name_branch, $branchID); //Берём название ветки
  array_push($arrTasks, $branchID[1]);
  array_push($arrMessages, $message);
}

for($i = 0; $i < count($arrTasks); $i++){     //Цикл рассылки тасков из массивов
$fields = array(
    'taskId' => $arrTasks[$i],
    'POST_MESSAGE' => $arrMessages[$i],
  );
$url = 'https://b24-v1vsle.bitrix24.ru/rest/'.$email_id.'/'.$token.'/task.ctaskcomments.add.json';   //task.ctaskcomments.add уникальная ссылка для каждого юзера
$ch = curl_init(); 
curl_setopt ($ch, CURLOPT_URL, $url); 
curl_setopt ($ch, CURLOPT_POST, true);
curl_setopt ($ch, CURLOPT_POSTFIELDS, $fields); 
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$post = curl_exec ($ch); 
curl_close ($ch); 
}

?>